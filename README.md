Payever Store Microservice
===

This application is build using the **[API Platform Framework][1]**.

Prerequisites
---
- **[GIT][3]**
- **[Docker][2]**


Setup (Docker)
---
```
git clone https://bitbucket.org/victorvasiloi/payever-store.git
cd payever-store
docker-compose up -d
docker-compose exec web bin/console doctrine:schema:create
```
Load fixtures (optional): `docker-compose exec web bin/console doctrine:fixtures:load`

Access
---
Usually on [http://localhost:3002/app_dev.php/](), but the hostname may vary depending on your docker configuration

[1]: https://api-platform.com
[2]: https://docs.docker.com/engine/installation/
[3]: https://www.atlassian.com/git/tutorials/install-git