<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Store;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadStores implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $product = (new Store())
                ->setName('Store ' . $i)
                ->setBusinessId(random_int(1, 5));

            $manager->persist($product);
        }

        $manager->flush();
    }
}