<?php

namespace AppBundle\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class StoreDeletedSubscriber implements EventSubscriberInterface
{
    /**
     * @var ProducerInterface
     */
    private $producer;
    /**
     * @var string
     */
    private $routingKey = 'store.deleted';

    /**
     * StoreSubscriber constructor.
     * @param ProducerInterface $producer
     */
    public function __construct(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['storeDeleted', EventPriorities::POST_WRITE]
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function storeDeleted(GetResponseForControllerResultEvent $event): void
    {
        $storeId = $event->getRequest()->attributes->get('id');

        if (!(null !== $storeId && $event->getRequest()->isMethod(Request::METHOD_DELETE))) {
            return;
        }

        $this->producer->publish(json_encode(['storeId' => $storeId]), $this->routingKey);
    }
}